FROM node:19-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install express dotenv body-parser request ejs

COPY . .

EXPOSE 5000

CMD ["npm", "run", "start"]
